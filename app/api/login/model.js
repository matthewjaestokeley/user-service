// the validation schema for the request object
var Joi = require('joi');
/**
 *
 * @type {Object}
 */
var signup = {
    /**
     * @type {Object}
     */
    post: {
        /**
         * @type {Object}
         */
        body: {
            /**
             * @type {Int}
             */
            username: Joi.string().alphanum().min(1).max(14).required(),
            password: Joi.string().min(1).max(14).required()
        }
    }
};

// Use the Joi object to create a few schemas for your routes.
module.exports = signup;

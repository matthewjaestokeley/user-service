/**
 * @type {Object}
 */
var hash = require('../../configuration/authentication/hash');
var config = require('../../configuration/authentication/config');
var token = require('../../configuration/authentication/token');

var login = {
    /**
     * Method handling the get request
     * @param  {Object}   collection \MongoCollection
     * @param  {Object}   req        request object
     * @param  {Object}   res        response object
     * @param  {Function} next       next object
     * @return {Null}                null
     */
    post: function(req, res, next, done) {

        var mongo = require('mongodb').MongoClient;
        // Connection URL
        var url = 'mongodb://localhost:27017/scribble';
        // Use connect method to connect to the Server
        mongo.connect(url, function(err, db) {

            var collection = db.collection('users');
            collection.findOne(
            {
                "username": req.body.username

            }, function(err, result) {
                if (err) {
                    return next(err);
                }

                if (!result) {
                    res.json({
                        "error": "Unknown User"
                    });
                    return next(err);
                }

                if (Object.keys(result).length === 0 ) {
                    res.json({
                        "error": "No User Data"
                    });
                    return next(err);
                }

                var hashed = hash(req.body.password, config.hmac.key);
                if (hashed !== result.password) {
                    res.json({
                        "error": "Invalid Password"
                    });
                }

                // @todo return user web token
                res.json({
                    "username": req.body.username,
                    "token": token(result._id.toString())
                });

            });

        });
    }
};

module.exports = login;

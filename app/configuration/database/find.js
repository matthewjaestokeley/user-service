var config = require('./config');
var mongo = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

var find = function(doc, collectionName, eventName) {

    if (typeof doc !== 'object') {
        return false;
    }

    if (typeof collectionName !== 'string') {
        return false;
    }

    mongo.connect(config.url, function(err, db) {
        var collection = db.collection(collectionName);
        collection.find(doc).toArray(function(err, result) {
            if (err) {
                res.json(JSON.stringify({
                    "error": "No Result"
                }));
                return next(err);
            }

            if (!result) {
                res.json(JSON.stringify({
                    "error": "Unknown User"
                }));
                return next(err);
            }

            if (Object.keys(result).length === 0 ) {
                res.json(JSON.stringify({
                    "error": "No User Data"
                }));
                return next(err);
            }
            res.json(result);
        });
    });
};


module.exports = find;
var config = require('./config');
var mongo = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var token = require('./../authentication/token');
var config = require('./../authentication/config');
var hash = require('./../authentication/hash');
//var query =  { "username": req.body.username };

var findOne = function(query, collectionName, eventName, req) {
    mongo.connect(config.url, function(err, db) {
        var collection = db.collection();
        collection.findOne(
            query
            , function(err, result) {
            if (err) {
                return next(err);
            }

            if (!result) {
                res.json({
                    "error": "Unknown User"
                });
                return next(err);
            }

            if (Object.keys(result).length === 0 ) {
                res.json({
                    "error": "No User Data"
                });
                return next(err);
            }

            // @todo create middleware
            var hashed = hash(req.body.password, config.hmac.key);
            if (hashed !== result.password) {
                res.json({
                    "error": "Invalid Password"
                });
            }

             // if (err) {
             //        res.json(JSON.stringify({
             //            "error": "No Result"
             //        }));
             //        return next(err);
             //    }

             //    if (!result) {
             //        res.json(JSON.stringify({
             //            "error": "Unknown User"
             //        }));
             //        return next(err);
             //    }

             //    if (Object.keys(result).length === 0 ) {
             //        res.json(JSON.stringify({
             //            "error": "No User Data"
             //        }));
             //        return next(err);
             //    }

            // @todo move the response to an option middleware that overrides returned result
            res.json({
                "username": req.body.username,
                "token": token(result._id.toString())
            });

        });
    });
};
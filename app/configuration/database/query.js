var config = require('./config');
var mongo = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;
var token = require('./../authentication/token');
var config = require('./../authentication/config');
var hash = require('./../authentication/hash');

// @ todo implement custom middleware api for chaining queries and handling errors
var catchErrors = function() {
};

var implementCallbacks = function() {
	var args = Array.splice(arguments, 2, 0);
	var middleware = Array.splice(arguments, arguments.length, -1);
	middleware.forEach(function(method) {
		method.apply(this, args);
	}.bind(this));
};

var query = {

	/**
	 * [abstract description]
	 * @return {[type]} [description]
	 */
	abstract: function() {
		implementCallbacks.call(this);
	},

	/**
	 * [find description]
	 * @param  {[type]} query          [description]
	 * @param  {[type]} collectionName [description]
	 * @return {[type]}                [description]
	 */
	find: function(query, collectionName) {

	},

	/**
	 * [findOne description]
	 * @param  {[type]} query          [description]
	 * @param  {[type]} collectionName [description]
	 * @param  {[type]} eventName      [description]
	 * @param  {[type]} req            [description]
	 * @return {[type]}                [description]
	 */
	findOne: function(query, collectionName, eventName, req) {

	},

	deleteOne: function() {

	}

};

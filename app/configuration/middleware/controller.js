//var app = require('express')();
//var router = express.Router();

exports.controller = function(controller) {

	var load = function(req, res, next) {
		controller(req, res, next);
	}

	return load; 
};
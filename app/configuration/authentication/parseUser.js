var decode = require('../../configuration/authentication/user');

modules.export = (function(token) {

	var getResponse = function(token) {
		return decode(token);
	};

	var getUser = function(response) {
		return response.iss;
	};

	var response = getResponse(token);
	var id = getUser(response);

	return {
		response: response,
		id: id
	}

})();
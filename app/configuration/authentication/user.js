var jwt = require('jwt-simple');
var config = require('./config');

var user = function(token) {

	return jwt.decode(token, config.jwt.key);

};

module.exports = user;
var moment = require('moment');
var jwt = require('jwt-simple');
var config = require('./config');

var token = function(userId) {

	var expires = moment().add('days', 7).valueOf();
	return jwt.encode({
		iss: userId,
		exp: expires
	}, config.jwt.key);

};

module.exports = token;
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var db = require('./../database/database');
var config = require('./config');
var hash = require('./hash');

module.exports = function () {


    // use local strategy
    passport.use(new LocalStrategy({
        usernameField: 'username',
        passwordField: 'password'
      },
      function(username, password, done) {
        var mongo = require('mongodb').MongoClient;

            // Connection URL
            var url = 'mongodb://localhost:27017/scribble';
            // Use connect method to connect to the Server
            mongo.connect(url, function(err, db) {
        //hash(password, config.hmac.key)
                var collection = db.collection('users');
                collection.findOne(
                {
                    "username": username

                }, function(err, result) {
                    if (err) {
                        res.json(JSON.stringify({
                            "error": "no result"
                        }));
                        return next(err);
                    }

                    if (!result) {
                      return done(null, false, { message: 'Unknown user' });
                    }

                    if (Object.keys(result).length === 0 ) {
                      return done(null, false, { message: 'No user data' });
                    }

                     var hashed = hash(password, config.hmac.key);
                      if (hashed !== result.password) {
                        return done(null, false, { message: 'Invalid password' });
                      }
                      return done(null, result);

                });

              });

      }
  ));

};
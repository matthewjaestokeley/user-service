var express = require("express");
var configuration = require('./configuration');
var app = express();

configuration.init(app);

app.use("/api", require("./api/notes/router"));
app.use("/api", require("./api/note/router"));
app.use("/api", require("./api/signup/router"));
app.use("/api", require("./api/login/router"));
app.use("/api", require("./api/save/router"));
app.use("/api", require("./api/user/router"));
app.use("/api", require("./api/delete/router"));

app.use(require("./site/note/router"));
app.use(require("./site/notes/router"));
app.use(require("./site/new/router"));
app.use(require("./site/unauthorized/router"));
app.use(require("./site/tour/router"));

// FINALLY, use any error handlers
configuration.handleErrors(app);

// Export the app instance for unit testing via supertest
module.exports = app;